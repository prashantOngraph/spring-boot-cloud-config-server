package com.ongraph.springcloudserver

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.config.server.EnableConfigServer

@EnableConfigServer
@SpringBootApplication
class SpringCloudServerApplication {

	static void main(String[] args) {
		SpringApplication.run(SpringCloudServerApplication, args)
	}

}
